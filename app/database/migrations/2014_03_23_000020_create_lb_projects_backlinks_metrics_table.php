<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbProjectsBacklinksMetricsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_projects_backlinks_metrics', function($t)
		{
			$t->increments('id');
			$t->integer('project_backlink_id')->unsigned()->index();
			$t->string('anchor_text');
			$t->string('anchor_title');
			$t->string('image_title')->nullable();
			$t->string('image_alt')->nullable();
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_projects_backlinks_metrics');
	}

}
