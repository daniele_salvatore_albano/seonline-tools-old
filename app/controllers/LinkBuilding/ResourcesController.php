<?php namespace LinkBuilding;

use Auth;
use Input;
use Redirect;
use Request;
use Response;
use View;

use LinkBuilding\Resource;

use BaseController;

class ResourcesController extends BaseController {

    public function index()
    {
        // Instanzia il modello
        $resourceModel = new Resource();

        // Instanzia la query
        $resourcesQuery = $resourceModel->newQuery();

        // Filtra per id utente
        $resourcesQuery->where('user_id', $this->me->id);

        // Se è stato passato il filtro di ricerca lo applica
        if (Input::has('search'))
        {
            $resourcesQuery->where('name', 'LIKE', '%' . Input::get('search') . '%');
            $resourcesQuery->orWhere('url', 'LIKE', '%' . Input::get('search') . '%');
            $resourcesQuery->orWhere('type', 'LIKE', '%' . Input::get('search') . '%');
        }

        // Applica l'ordinamento
        $resourcesQuery->orderBy('name', 'asc');

        // Esegue la paginazione
        $resources = $resourcesQuery->paginate(Input::get('rows', '10'));

        // Renderizza la view
        $this->layout->content = View::make('linkbuilding/resources/index')->with('resources', $resources);
    }

    public function add()
    {
        if ($result = $this->handleAddOrEdit()) return $result;

        $this->layout->content = View::make('linkbuilding/resources/edit')->with('resource', null);
    }

    public function edit($id)
    {
        $resource = Resource::findOrFail($id);

        if ($result = $this->handleAddOrEdit($resource)) return $result;

        $this->layout->content = View::make('linkbuilding/resources/edit')->with('resource', $resource);
    }

    public function delete($id)
    {
        $resource = Resource::findOrFail($id);
        $resource->delete();

        return Redirect::route('linkbuilding_resources_list')->with('status', 'Resource successfully deleted');
    }

    public function disable($id)
    {
        $resource = Resource::findOrFail($id);
        $resource->enabled = false;
        $resource->save();

        return Redirect::route('linkbuilding_resources_list')->with('status', 'Resource successfully disabled');
    }

    public function enable($id)
    {
        $resource = Resource::findOrFail($id);
        $resource->enabled = true;
        $resource->save();

        return Redirect::route('linkbuilding_resources_list')->with('status', 'Resource successfully enabled');
    }

    private function handleAddOrEdit($resource = null)
    {
        if (Request::isMethod('post'))
        {
            $input = Input::all();

            if (preg_match('#^\w://#i', $input['url']) == false)
            {
                $input['url'] = 'http://' . $input['url'];
            }

            if ($validator = Resource::validate($input))
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            if ($resource == null)
            {
                $resource = new Resource();
                $resource->user_id = $this->me->id;
            }

            // Scompone la url per ricostruirla correttamente
            $input['url'] = preg_replace('#^\w+://#i', '', $input['url']);
            $urlParts = parse_url($input['url']);
            $input['url'] = $urlParts['host'] . (isset($urlParts['path']) ? $urlParts['path'] : '');

            // Imposta i campi della risorsa
            $resource->name = $input['name'];
            $resource->type = $input['type'];
            $resource->require_registration = $input['registration'];
            $resource->notes = $input['notes'];
            $resource->enabled = $input['enabled'];
            $resource->last_update = $resource->url != $input['url'] ? 0 : $resource->last_update;
            $resource->url = $input['url'];
            $resource->save();

            // Effettua il redirect
            return Redirect::route('linkbuilding_resources_edit', array('id' => $resource->id))
                ->with('status', 'Resource successfully saved');
        }        
    }

    protected function getLayoutPath()
    {
        if (parent::getLayoutPath() == false)
        {
            return false;
        }

        return $this->viewPath('layouts/backend');
    }
}