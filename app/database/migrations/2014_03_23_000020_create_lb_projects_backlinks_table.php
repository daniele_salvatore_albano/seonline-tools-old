<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbProjectsBacklinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_projects_backlinks', function($t)
		{
			$t->increments('id');
			$t->integer('resource_id')->unsigned()->index();
			$t->integer('project_keyword_id')->unsigned()->index();
			$t->string('url');
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_projects_backlinks');
	}

}
