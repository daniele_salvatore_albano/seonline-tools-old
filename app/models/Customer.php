<?php

class Customer extends Eloquent {

	protected $table = 'customers';
	protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function projects()
    {
        return $this->hasMany('Project');
    }

	public static function validate($input)
	{
    	$rules = array
    	(
    		'name' 		=> array('required', 'min:4'),
    		'email' 	=> array('required', 'email'),
    		'enabled'	=> array('required', 'in:0,1'),
		);

		$messages = array();

    	$validator = Validator::make($input, $rules, $messages);

    	if ($validator->fails())
    	{
    		return $validator;
    	}
	}
}