<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($t)
		{
			$t->increments('id');
			$t->string('email')->index();
			$t->string('password');
			$t->string('display_name');
			$t->string('first_name')->nullable();
			$t->string('last_name')->nullable();
			$t->string('avatar_url')->nullable();
			$t->string('oauth_facebook_id')->nullable()->index();
			$t->string('oauth_google_id')->nullable()->index();
			$t->string('oauth_linkedin_id')->nullable()->index();
			$t->boolean('validated');
			$t->string('validation_token')->nullable()->index();
			$t->boolean('enabled');
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
