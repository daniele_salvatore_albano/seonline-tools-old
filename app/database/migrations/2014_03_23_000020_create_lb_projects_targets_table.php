<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbProjectsTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_projects_targets', function($t)
		{
			$t->increments('id');
			$t->integer('project_id')->unsigned()->index();
			$t->enum('type', array('KeywordRank', 'BacklinksCount', 'PageAuthority', 'DomainAuthority'));
			$t->integer('project_keyword_id')->unsigned()->index()->nullable();
			$t->integer('backlinks_count')->unsigned()->nullable();
			$t->boolean('page_authority')->unsigned()->nullable();
			$t->boolean('domain_authority')->unsigned()->nullable();
			$t->boolean('enabled');
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_projects_targets');
	}

}
