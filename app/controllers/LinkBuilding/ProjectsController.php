<?php namespace LinkBuilding;

use Auth;
use Input;
use Redirect;
use Request;
use View;
use BaseController;

class ProjectsController extends BaseController {

    public function index()
    {
        // Instanzia il modello
        $projectModel = new Project();

        // Instanzia la query
        $projectsQuery = $projectModel->newQuery();

        // Si aggancia ai customer
        $projectsQuery->with(array
        (
            'customer' => function($query)
            {
                // Estrae i clienti legati all'utente corrente
                $query->where('user_id', $this->me->id);

                if (Input::has('search'))
                {
                    $query->where('name', 'LIKE', '%' . Input::get('search') . '%');
                }
            }
        ));

        // Se è stato passato il filtro di ricerca lo applica
        if (Input::has('search'))
        {
            $projectsQuery->orWhere('customers.name', 'LIKE', '%' . Input::get('search') . '%');
        }

        // Applica l'ordinamento
        $projectsQuery->orderBy('name', 'asc');

        // Esegue la paginazione
        $projects = $projectsQuery->paginate(Input::get('rows', '10'));

        // Renderizza la view
        $this->layout->content = View::make('linkbuilding/projects/index')->with('projects', $projects);
    }

    public function add()
    {
        if ($result = $this->handleAddOrEdit()) return $result;

        $this->layout->content = View::make('linkbuilding/projects/edit')->with('project', null);
    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);

        if ($result = $this->handleAddOrEdit($project)) return $result;

        $this->layout->content = View::make('linkbuilding/projects/edit')->with('project', $project);
    }

    public function delete($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();

        return Redirect::route('projects_list')->with('status', 'Project successfully deleted');
    }

    public function disable($id)
    {
        $project = Project::findOrFail($id);
        $project->enabled = false;
        $project->save();

        return Redirect::route('projects_list')->with('status', 'Project successfully disabled');
    }

    public function enable($id)
    {
        $project = Project::findOrFail($id);
        $project->enabled = true;
        $project->save();

        return Redirect::route('projects_list')->with('status', 'Project successfully enabled');
    }

    private function handleAddOrEdit($project = null)
    {
        if (Request::isMethod('post'))
        {
            if ($validator = Project::validate(Input::all()))
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            if ($project == null)
            {
                $project = new Project();
            }

            $project->customer_id = Input::get('customer');
            $project->name = Input::get('name');
            $project->notes = Input::get('notes');
            $project->enabled = Input::get('enabled');
            $project->save();

            return Redirect::route('projects_edit', array('id' => $project->id))->with('status', 'Project successfully saved');
        }        
    }

    protected function getLayoutPath()
    {
        if (parent::getLayoutPath() == false)
        {
            return false;
        }

        return $this->viewPath('layouts/backend');
    }
}