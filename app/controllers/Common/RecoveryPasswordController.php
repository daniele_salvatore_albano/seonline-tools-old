<?php namespace Common;

use Hash;
use Input;
use Lang;
use Password;
use Redirect;
use Request;
use View;
use BaseController;

class RecoveryPasswordController extends BaseController {

	public function remind()
	{
		if (Request::isMethod('post'))
		{
			$response = Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('Password Reminder');
			});

			switch ($response)
			{
				case Password::INVALID_USER:
					return Redirect::back()->withInput()->with('error', Lang::get($response));

				case Password::REMINDER_SENT:
					return Redirect::back()->withInput()->with('status', Lang::get($response));
			}
		}

		$this->layout->content = View::make('common/recovery-password/remind');
	}

	public function reset()
	{
		if (Request::isMethod('post'))
		{
			$credentials = Input::only('email', 'password', 'password_confirmation', 'token');

			$response = Password::reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);

				$user->save();
			});

			switch ($response)
			{
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					return Redirect::back()->withInput()->with('error', Lang::get($response));

				case Password::PASSWORD_RESET:
					return Redirect::to('/');
			}
		}

		if (Input::has('token') == false)
		{
			App::abort(404);
		}

		$this->layout->content = View::make('common/recovery-password/reset')->with('token', Input::get('token'));
	}
}