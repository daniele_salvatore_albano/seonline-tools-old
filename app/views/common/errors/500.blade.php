<?php

$__env->__body_class = isset($__env->__body_class)
    ? (isset($__env->__body_class_add)
            ? array_merge(array('contrast-red error contrast-background'), $__env->__body_class) : $__env->__body_class)
    : array('contrast-red error contrast-background');

?>

@section('title')
@parent
|
Error 404
@stop

@section('content')
<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='error-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <i class='icon-exclamation-sign'></i>
                                500
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='error-container'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-4 col-sm-offset-4'>
                            <h4 class='text-center title'>
                                Ooops! Something went wrong, we'll fix it soon.
                            </h4>
                            <div class='text-center'>
                                <a class='btn btn-md btn-ablock' href='index.html'>
                                    <i class='icon-chevron-left'></i>
                                    Go back
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='error-container-footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <img width="121" height="31" alt="Flatty" src="assets/images/logo_lg.svg" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop