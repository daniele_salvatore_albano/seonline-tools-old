<?php namespace LinkBuilding;

use Auth;
use Input;
use Redirect;
use Request;
use View;
use BaseController;

class ProjectsResourcesController extends BaseController {

    public function index()
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/index');
    }

    public function add()
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/add');
    }

    public function edit($id, $id2)
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/edit');
    }

    public function delete()
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/delete');
    }

    public function disable()
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/disable');
    }

    public function enable()
    {
        $this->layout->content = View::make('linkbuilding/projectsresources/enable');
    }

    protected function getLayoutPath()
    {
    	if (parent::getLayoutPath() == false)
    	{
    		return false;
    	}

    	return $this->viewPath('layouts/backend');
    }
}