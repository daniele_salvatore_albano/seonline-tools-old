<?php

$__env->__body_class = isset($__env->__body_class)
    ? (isset($__env->__body_class_add)
            ? array_merge(array('contrast-red login contrast-background'), $__env->__body_class) : $__env->__body_class)
    : array('contrast-red login contrast-background');

?>

@section('title')
@parent
|
Login
@stop

@section('content')
<div class="middle-container">
    <div class="middle-row">
        <div class="middle-wrapper">
            <div class="login-container-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <img width="121" height="31" src="assets/images/logo_lg.svg" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <h1 class="text-center title">Sign in</h1>

                            @if (Session::has('status'))
                            <div class="alert alert-success alert-dismissable">
                            <a class="close" data-dismiss="alert" href="#">×</a>
                            <i class="icon-remove-sign"></i>
                            {{ Session::get('status') }}
                            </div>                            
                            @endif

                            @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                            <a class="close" data-dismiss="alert" href="#">×</a>
                            <i class="icon-remove-sign"></i>
                            {{ Session::get('error') }}
                            </div>                            
                            @endif

                            <form action="{{ URL::route('login') }}" class="validate-form" method="post">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input value="{{{ Input::old('email') }}}" placeholder="E-mail" class="form-control" data-rule-required="true" name="email" type="text" />
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input value="" placeholder="Password" class="form-control" data-rule-required="true" name="password" type="password" />
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <div class="checkbox">
                                    <label for="remember_me">
                                        <input id="remember_me" name="remember_me" type="checkbox" value="1">
                                        Remember me
                                    </label>
                                </div>
                                <button class="btn btn-block">Sign in</button>
                            </form>
                            <div class="text-center">
                                <hr class="hr-normal">
                                Login using<br />
                                <a href="{{ URL::route('login-using-facebook') }}">Facebook</a>
                                <a href="{{ URL::route('login-using-google') }}">Google</a>
                                <a href="{{ URL::route('login-using-twitter') }}">Twitter</a>
                                <a href="{{ URL::route('login-using-linkedin') }}">Linkedin</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <a href="{{ URL::route('register') }}">
                                    <i class="icon-user"></i>
                                    New to Flatty?
                                    <strong>Sign up</strong>
                                </a>
                                -
                                <a href="{{ URL::route('recovery-password') }}">
                                    <i class="icon-lock"></i>
                                    Forgot your password?
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop