<?php namespace Acme\Support\Seo;

class Google
{
	public static function getPageRank($url)
	{
		$query = 'http://toolbarqueries.google.com/tbr?client=navclient-auto&ch=' . self::checkHash(self::hashUrl($url)) . '&features=Rank&q=info:' . $url . '&num=100&filter=0';
		$data = file_get_contents($query);
		$pos = strpos($data, 'Rank_');
		
		if ($pos === false)
		{
			return null;
		}

		return (int)trim(substr($data, $pos + 9));
	}

	private static function strToNum($str, $check, $magic)
	{
		$int32Unit = 4294967296; // 2^32

		$length = strlen($str);
		for ($i = 0; $i < $length; $i++)
		{
			$check *= $magic;

			if ($check >= $int32Unit)
			{
				$check = ($check - $int32Unit * (int) ($check / $int32Unit));
				$check = ($check < -2147483648) ? ($check + $int32Unit) : $check;
			}
			
			$check += ord($str{$i});
		}
		
		return $check;
	}

	private static function hashUrl($url)
	{
		$check1 = self::strToNum($url, 0x1505, 0x21);
		$check2 = self::strToNum($url, 0, 0x1003F);

		$check1 >>= 2;
		$check1 = (($check1 >> 4) & 0x3FFFFC0 ) | ($check1 & 0x3F);
		$check1 = (($check1 >> 4) & 0x3FFC00 ) | ($check1 & 0x3FF);
		$check1 = (($check1 >> 4) & 0x3C000 ) | ($check1 & 0x3FFF);

		$t1 = (((($check1 & 0x3C0) << 4) | ($check1 & 0x3C)) <<2 ) | ($check2 & 0xF0F );
		$t2 = (((($check1 & 0xFFFFC000) << 4) | ($check1 & 0x3C00)) << 0xA) | ($check2 & 0xF0F0000 );

		return ($t1 | $t2);
	}

	private static function checkHash($hashnum)
	{
		$checkByte = 0;
		$flag = 0;

		$hashStr = sprintf('%u', $hashnum) ;
		$length = strlen($hashStr);

		for ($i = $length - 1; $i >= 0; $i --)
		{
			$Re = $hashStr{$i};
			if (1 === ($flag % 2))
			{
				$Re += $Re;
				$Re = (int)($Re / 10) + ($Re % 10);
			}
			$checkByte += $Re;
			$flag ++;
		}

		$checkByte %= 10;
		if (0 !== $checkByte)
		{
			$checkByte = 10 - $checkByte;
			if (1 === ($flag % 2) )
			{
				if (1 === ($checkByte % 2))
				{
					$checkByte += 9;
				}
				
				$checkByte >>= 1;
			}
		}

		return '7' . $checkByte . $hashStr;
	}
}