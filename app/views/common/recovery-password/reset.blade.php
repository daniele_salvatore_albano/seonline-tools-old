<?php

$__env->__body_class = isset($__env->__body_class)
    ? (isset($__env->__body_class_add)
            ? array_merge(array('contrast-red recovery-password contrast-background'), $__env->__body_class) : $__env->__body_class)
    : array('contrast-red recovery-password contrast-background');

?>

@section('title')
@parent
|
Recovery Password
@stop

@section('content')
<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='recovery-password-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <img width="121" height="31" src="assets/images/logo_lg.svg" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='recovery-password-container'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-4 col-sm-offset-4'>
                            <h1 class="text-center title">Recovery Password</h1>

                            @if (Session::has('status'))
                            <div class="alert alert-success alert-dismissable">
                            <a class="close" data-dismiss="alert" href="#">×</a>
                            <i class="icon-remove-sign"></i>
                            {{ Session::get('status') }}
                            </div>                            
                            @endif

                            @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                            <a class="close" data-dismiss="alert" href="#">×</a>
                            <i class="icon-remove-sign"></i>
                            {{ Session::get('error') }}
                            </div>                            
                            @endif

                            <form action="{{ URL::route('recovery-password-reset') }}" class="validate-form" method="post">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class='form-group'>
                                    <div class='controls with-icon-over-input'>
                                        <input value="{{{ Input::old('email') }}}" placeholder="E-mail" class="form-control" data-rule-required="true" name="email" type="text" />
                                        <i class='icon-user text-muted'></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input value="" placeholder="Password" class="form-control" data-rule-required="true" name="password" type="password" />
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input value="" placeholder="Password Confirmation" class="form-control" data-rule-required="true" name="password_confirmation" type="password" />
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <button class='btn btn-block'>Change Password</button>
                            </form>
                            <div class='text-center'>
                                <hr class='hr-normal'>
                                <a href='{{ URL::route('login') }}'>Do you want to login?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='recovery-password-container-footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <a href='{{ URL::route('register') }}'>
                                    <i class='icon-user'></i>
                                    New to Seo Tools?
                                    <strong>Sign up</strong>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop