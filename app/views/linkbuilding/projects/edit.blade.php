@extends('common/projects/edit')

@section('page-breadcrumb')
<li class="active">
    <h1>
        <a href="{{ URL::route('linkbuilding_resources_list') }}">
            <i class="icon-download-alt"></i>
            <span>Projects</span>
        </a>
    </h1>
</li>
<li class="separator"><i class="icon-angle-right"></i></li>
@if ($project)
<li class="active">Edit</li>
@else
<li class="active">Add New</li>
@endif
@stop

@section('edit-form')
@parent
<div class="row">
    <div class="col-sm-6">
        <div class="box keywords">
            <div class="box-header green-background">
                <div class="title">Keywords</div>
            </div>
            <div class="box-content">

                <div>
                    <div class="form-group">
                        <input class="form-control input-sm keyword" type="hidden" name="keywords_id[]" value="new" />
                        <div class="col-md-4">
                            <input class="form-control input-sm keyword" placeholder="Keyword" name="keywords_keyword[]" />
                        </div>
                        <div class="col-md-7">
                            <input class="form-control input-sm url" placeholder="Url" name="keywords_url[]" />
                        </div>
                        <div class="col-sm-1" style="line-height: 2em;">
                            <a href="#" class="delete text-red"><i class="icon-trash icon-large"></i></a>
                        </div>
                    </div>
                </div>

                <div class="form-actions form-actions-padding-sm">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-2">
                            <button class="btn btn-primary" type="submit">
                                <i class="icon-save"></i>
                                Save
                            </button>
                            <a class="btn" type="submit" href="{{ URL::route('projects_list') }}">Back</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="box">
            <div class="box-header green-background">
                <div class="title">Targets</div>
            </div>
            <div class="box-content">

                <div class="form-group">
                    <div class="col-md-4">
                        <select class="form-control input-sm" type="text" name="targets_type[]">
                            <option value="">Type...</option>
                            <option value="KeywordRank">Keyword Rank</option>
                            <option value="BacklinksCount">Backlinks Count</option>
                            <option value="PageAuthority">Page Authority</option>
                            <option value="DomainAuthority">Domain Authority</option>
                        </select>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control input-sm" placeholder="Url" type="text" name="targets_value[]" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop

@section('js-after')
@parent
<script type="text/javascript">
$(function()
{
    $('.keywords')
        .on('click', 'DIV.form-group A.delete', function(e)
        {
            $me = $(this);
            $formGroup = $me.parents('.form-group:eq(0)');

            if ($('.keywords DIV.form-group').length == 1)
            {
                $formGroup.find('INPUT.keyword').val('');
                $formGroup.find('INPUT.url').val('');
            }
            else
            {
                $formGroup.remove();
            }

            e.preventDefault();
            return false;
        })
        .on('keydown', 'DIV.form-group INPUT', function(e)
        {
            $me = $(this);
            $formGroup = $me.parents('.form-group:eq(0)');

            if ($formGroup.is(':last-child') == false)
            {
                return;
            }

            $newFormGroup = $formGroup.clone();
            $newFormGroup.find('INPUT.keyword').val('');
            $newFormGroup.find('INPUT.url').val('');

            $newFormGroup.insertAfter($formGroup);            
        })
        .on('paste', 'DIV.form-group INPUT', function(e)
        {
            e.preventDefault();
            $me = $(this);
            $formGroup = $me.parents('.form-group:eq(0)');
            $lastFormGroup = $formGroup;

            var text = false;

            if (!!((e.originalEvent || e).clipboardData))
            {
                text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text/plain');
            }
            else if (!!window.clipboardData)
            {
                text = window.clipboardData.getData('Text');
            }

            if (text == false)
            {
                alert('Unable to import pasted data, try again!');
                return;
            }

            // Split lines
            var lines = text.split(/\n/g);

            // Split first line to check the number of columns
            var columnsCheck = lines[0].split(/(\t|,|;)/g);

            // Check if there is only one line and one column, in this case it is a simple paste
            if (columnsCheck.length == 1 && (lines.length == 1 || (lines.length == 2 && lines[1] == '')))
            {
                // Put the value
                $me.val(text);
                return;
            }

            // We need two columns
            if (columnsCheck.length < 2)
            {
                alert('The pasted content contains only one column, we need two columns');
                return;
            }

            // Loop lines
            var isFirstLine = true;
            for(var lineIndex in lines)
            {
                // Acquire the line
                var line = lines[lineIndex];

                // Split columns
                var columns = line.split(/\t/g);

                if (columns[0].length == 0)
                {
                    continue;
                }

                // If it is the first line, it update the current row
                if (isFirstLine)
                {
                    $formGroup.find('INPUT.keyword').val(columns[0]);
                    $formGroup.find('INPUT.url').val(columns.length > 1 ? columns[1] : '');

                    isFirstLine = false;
                }
                else
                {
                    $newFormGroup = $lastFormGroup.clone();
                    $newFormGroup.find('INPUT.keyword').val(columns[0]);
                    $newFormGroup.find('INPUT.url').val(columns.length > 1 ? columns[1] : '');

                    $newFormGroup.insertAfter($lastFormGroup);

                    $lastFormGroup = $newFormGroup;
                }


            }
        });
})
</script>
@stop