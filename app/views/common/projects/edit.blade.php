<?php $counter = 0; ?>

@section('page-breadcrumb')
<li class="active">
    <h1>
        <a href="{{ URL::route('projects_list') }}">
            <i class="icon-download-alt"></i>
            <span>Projects</span>
        </a>
    </h1>
</li>
<li class="separator"><i class="icon-angle-right"></i></li>
@if ($project)
<li class="active">Edit</li>
@else
<li class="active">Add New</li>
@endif
@stop

@section('page-content')
<div class="row">
    <div class="col-sm-12">

        @if (Session::has('status'))
        <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <i class="icon-remove-sign"></i>
        {{ Session::get('status') }}
        </div>                            
        @endif

        <form class="form form-horizontal" style="margin-bottom: 0;" method="post">
            @section('edit-form')
                <div class="box">
                    <div class="box-header green-background">
                        <div class="title">
                            @if ($project)
                            <div class="icon-edit"></div>
                            Edit Project &laquo;{{{ $project->name }}}&raquo;
                            @else
                            <div class="icon-plus"></div>
                            Add New Project
                            @endif
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="form-group {{ $errors->has('customer') ? 'has-error' : '' }}">
                            <label class="col-md-2 control-label" for="customer">Customer</label>
                            <div class="col-md-5">
                                <input class="form-control" id="customer" name="customer" value="{{{ Input::old('customer', $project ? $project->customer_id : '') }}}">
                                {{ implode("\r\n", $errors->get('customer', '<span class="help-block">:message</span>')) }}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="col-md-2 control-label" for="name">Name</label>
                            <div class="col-md-5">
                                <input class="form-control" placeholder="Name" type="text" id="name" name="name" value="{{{ Input::old('name', $project ? $project->name : '') }}}">
                                {{ implode("\r\n", $errors->get('name', '<span class="help-block">:message</span>')) }}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
                            <label class="col-md-2 control-label" for="notes">Notes</label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="notes" name="notes" placeholder="Notes" rows="3">{{{ Input::old('Notes', $project ? $project->notes : '') }}}</textarea>
                                {{ implode("\r\n", $errors->get('notes', '<span class="help-block">:message</span>')) }}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('enabled') ? 'has-error' : '' }}">
                            <label class="col-md-2 control-label">Enabled</label>
                            <div class="col-md-10">
                                <label class="radio radio-inline">
                                    <input type="radio" name="enabled" value="1" {{ Input::old('enabled', $project ? $project->enabled : '1') == '1' ? 'checked' : '' }}>
                                    Yes
                                </label>
                                <label class="radio radio-inline">
                                    <input type="radio" name="enabled" value="0" {{ Input::old('enabled', $project ? $project->enabled : '1') == '0' ? 'checked' : '' }}>
                                    No
                                </label>
                                {{ implode("\r\n", $errors->get('enabled', '<span class="help-block">:message</span>')) }}
                            </div>
                        </div>
                        <div class="form-actions form-actions-padding-sm">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="icon-save"></i>
                                        Save
                                    </button>
                                    <a class="btn" type="submit" href="{{ URL::route('projects_list') }}">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @show
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link href="{{ URL::asset('stylesheets/plugins/select2/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop

@section('js-after')
@parent
<script src="{{ URL::asset('javascripts/plugins/select2/select2.js') }}" type="text/javascript"></script>


<script type="text/javascript">
$(function()
{
    function formatResult(item)
    {
        if(!item.id)
        {
            return item.text;
        }
        
        return '<strong>' + item.row.name + '</strong><br /><small>' + item.row.email + '</small>';
    }

    function formatSelection(item)
    {
        return item.row.name;
    }    

    $("INPUT[name=customer]").select2(
    {
        formatResult: formatResult,
        formatSelection: formatSelection,
        placeholder: "Select a customer",
        data:
        [
            @foreach(Customer::where('enabled', '1')->orderBy('name', 'asc')->get() as $customer)
            { id: '{{ $customer->id }}', text: '{{{ $customer->name }}} {{{ $customer->email }}}', row: {{ $customer->toJson() }} },
            @endforeach
        ]
    });
})
</script>
@stop