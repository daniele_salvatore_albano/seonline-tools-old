@extends('layouts/master')

@section('header')
<header>
    <nav class="navbar navbar-default">
        <a class="navbar-brand" href="index.html">
            <img width="81" height="21" class="logo" alt="Flatty" src="assets/images/logo.svg">
            <img width="21" height="21" class="logo-xs" alt="Flatty" src="assets/images/logo_xs.svg">
        </a>
        <a class="toggle-nav btn pull-left" href="#">
            <i class="icon-reorder"></i>
        </a>
        <ul class="nav">
            <li class="dropdown light only-icon">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-cog"></i>
                </a>
                <ul class="dropdown-menu color-settings">
                </ul>
            </li>
            <li class="dropdown medium only-icon widget">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-rss"></i>
                    <div class="label">{{ Notification::where('user_id', '=', $me->id)->where('readed', '=', '0')->count() }}</div>
                </a>
                <ul class="dropdown-menu">
                    @foreach(($notifications = Notification::where('user_id', '=', $me->id)->orderBy('created_at', 'desc')->limit(5)->get()) as $notification)
                    <li>
                        <a href="{{ URL::route('me_notifications') }}/{{ $notification->id }}">
                            <div class="widget-body">
                                <div class="pull-left icon">
                                    @if ($notification->sender_user_id > 0)
                                    <i class="icon-user text-success"></i>
                                    @else
                                    <i class="icon-inbox text-error"></i>
                                    @endif
                                </div>
                                <div class="pull-left text">
                                    {{ $notification->message }}
                                    <small class="text-muted">
                                    @if ($notification->created_at->diffInDays() > 30)
                                    {{ $notification->created_at->toFormattedDateString() }}
                                    @else
                                    {{ $notification->created_at->diffForHumans() }}
                                    @endif
                                    </small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    @endforeach
                    <li class="widget-footer">
                    @if ($notifications->count() > 0)
                        <a href="{{ URL::route('me_notifications') }}">All notifications</a>
                    @else
                        <a href="#">There isn't any notification</a>
                    @endif
                    </li>
                </ul>
            </li>
            <li class="dropdown dark user-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <img width="23" height="23" alt="{{ $me->display_name }}" src="{{ $me->picture_url ? $me->picture_url : "http://www.gravatar.com/avatar/" . md5(strtolower(trim($me->email))) }}">
                    <span class="user-name">{{ $me->display_name }}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ URL::route('me_profile') }}">
                            <i class="icon-user"></i>
                            Profile
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('me_settings') }}">
                            <i class="icon-cog"></i>
                            Settings
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('me_billing') }}">
                            <i class="icon-cog"></i>
                            Billing
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ URL::route('logout') }}">
                            <i class="icon-signout"></i>
                            Sign out
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <form action="search_results.html" class="navbar-form navbar-right hidden-xs" method="get">
            <button class="btn btn-link icon-search" name="button" type="submit"></button>
            <div class="form-group">
                <input value="" class="form-control" placeholder="Search..." autocomplete="off" id="q_header" name="q" type="text">
            </div>
        </form>
    </nav>
</header>
@stop

@section('content')

<div id="wrapper">

    <div id="main-nav-bg"></div>

    <nav id="main-nav">
        <div class="navigation">
            <div class="search">
                <form action="search_results.html" method="get">
                    <div class="search-wrapper">
                        <input value="" class="search-query form-control" placeholder="Search..." autocomplete="off" name="q" type="text">
                        <button class="btn btn-link icon-search" name="button" type="submit"></button>
                    </div>
                </form>
            </div>
            <ul class="nav nav-stacked">
                <li class="">
                    <a href="{{ URL::route('dashboard') }}">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{ URL::route('customers_list') }}">
                        <i class="icon-user"></i>
                        <span>Customers</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{ URL::route('projects_list') }}">
                        <i class="icon-file-text-alt"></i>
                        <span>Projects</span>
                    </a>
                </li>

                <li>
                    <a class="dropdown-collapse" href="#">
                        <i class="icon-link"></i>
                        <span>Link Building</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>
                    <ul class="nav nav-stacked">
                        <li>
                            <a href="{{ URL::route('linkbuilding_resources_list') }}">
                                <i class="icon-download-alt"></i>
                                <span>Resources</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::route('linkbuilding_projects_list') }}">
                                <i class="icon-file-text-alt"></i>
                                <span>Projects</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>



    <section id="content">
        <div class="container">
            <div class="row" id="content-wrapper">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <div class="pull-left">
                                    <ul class="breadcrumb">
                                        @yield('page-breadcrumb')
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    @yield('page-content')

                </div>
            </div>
            <footer id="footer">
                <div class="footer-wrapper">
                    <div class="row">
                        <div class="col-sm-6 text">
                            Copyright © 2014-2015 Seo Tools
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>

</div>

@stop