<?php namespace Acme\Tasks\LinkBuilding;

use DB;
use PDO;
use Eloquent;

use Notification;
use LinkBuilding\Resource;

class UpdateNewResourcesStats
{
    public static function fire()
    {
        Eloquent::unguard();

        set_time_limit(15 * 60);

        $sql = "
        SELECT
            `lb_resources`.*

        FROM
            `lb_resources`

        INNER JOIN `users` ON
        (
            `users`.`id` = `lb_resources`.`user_id`
            AND
            `users`.`validated` = 1
            AND
            `users`.`enabled` = 1
        )
        
        WHERE
            `lb_resources`.`deleted_at` IS NULL
            AND
            `lb_resources`.`enabled` = 1
            AND
            `lb_resources`.`last_update` = 0";

        $statement = DB::getPdo()->prepare($sql);
        $statement->execute();

        $urlsToResources = array();
        $alternativeUrls = array();
        $originalUrls = array();

        while($row = $statement->fetch(PDO::FETCH_OBJ))
        {
            $urlParts = parse_url('http://' . $row->url);

            $url = $urlParts['host'] . (isset($urlParts['path']) ? $urlParts['path'] : '');

            if (isset($urlsToResources[$url]) == false)
            {
                $urlsToResources[$url] = array();
            }
            $urlsToResources[$url][] = $row;

            $urlHostParts = explode('.', $urlParts['host']);
            $generateUrl2 = false;

            if (count($urlHostParts) == 3 && strtolower($urlHostParts[0]) == 'www')
            {
                array_shift($urlHostParts);
                $generateUrl2 = true;
            }
            else if (count($urlHostParts) == 2)
            {
                array_unshift($urlHostParts, 'www');
                $generateUrl2 = true;
            }

            if ($generateUrl2)
            {
                $urlParts['host'] = implode('.', $urlHostParts);

                $url2 = $urlParts['host'] . (isset($urlParts['path']) ? $urlParts['path'] : '');

                if (isset($urlsToResources[$url2]) == false)
                {
                    $urlsToResources[$url2] = array();
                }

                $urlsToResources[$url2][] = $row;

                $alternativeUrls[$url2] = $url;
                $originalUrls[$url] = $url2;
            }

            unset($row);
        }

        if (count($urlsToResources) == 0)
        {
            return;
        }

        // Effettua la richiesta a mozscape
        $fields = array('title', 'mozrank', 'subdomain-mozrank', 'page-authority', 'domain-authority', 'time-last-crawled');
        $urls = array_keys($urlsToResources);

        // Effettua la query su mozscape
        $results = \Acme\Support\Seo\Mozscape::urlMetrics($fields, $urls);

        // Resetta l'array
        reset($results);

        // Cicla i risultati ottenuti
        while($values = each($results))
        {
            $url = key($results);

            if ($url == null)
            {
                continue;
            }

            $sendNotifications = false;

            // Verifica se è stata generata l'alternativa per l'rul
            if (isset($alternativeUrls[$url]) || isset($originalUrls[$url]))
            {
                $originalUrl = isset($alternativeUrls[$url]) ? $alternativeUrls[$url] : $url;
                $alternativeUrl = isset($originalUrls[$url]) ? $originalUrls[$url] : $url;

                $originalResult = $results[$originalUrl];
                $alternativeResult = $results[$alternativeUrl];

                // Calcola l'importanza dell'url tramite una media sui valori estratti
                $originalMidValue = floor(
                    ((float)$originalResult['mozrank'] +
                    (float)$originalResult['subdomain-mozrank'] +
                    (float)$originalResult['page-authority'] +
                    (float)$originalResult['domain-authority']) / 4);

                $alternativeMidValue = floor(
                    ((float)$alternativeResult['mozrank'] +
                    (float)$alternativeResult['subdomain-mozrank'] +
                    (float)$alternativeResult['page-authority'] +
                    (float)$alternativeResult['domain-authority']) / 4);

                // Verifica qual'è il più importante
                if ($originalMidValue >= $alternativeMidValue)
                {
                    unset($results[$alternativeUrl]);
                    $updateUrl = $originalUrl;
                }
                else if ($originalMidValue < $alternativeMidValue)
                {
                    unset($results[$originalUrl]);
                    $updateUrl = $alternativeUrl;

                    // Se viene utilizzato l'url alternativo, notifica l'utente
                    $sendNotifications = true;
                }

                $updateResult = $results[$updateUrl];
            }
            else
            {
                $updateResult = $values;
                $updateUrl = $url;
            }

            // Acquisisce il pagerank
            $pageRank = \Acme\Support\Seo\Google::getPageRank($updateUrl);

            // Aggiorna il db
            foreach($urlsToResources[$updateUrl] as $row)
            {
                // Aggiorna la risorsa
                $resource = new Resource((array)$row);
                $resource->exists = true;
                $resource->url = 'http://' . $updateUrl;
                $resource->pagerank = $pageRank;
                $resource->mozrank = $updateResult['mozrank'];
                $resource->mozrank_subdomain = $updateResult['subdomain-mozrank'];
                $resource->page_authority = $updateResult['page-authority'];
                $resource->domain_authority = $updateResult['domain-authority'];
                $resource->last_update = $updateResult['time-last-crawled'];
                $resource->save();

                // Verifica se va inviata la notifica all'utente
                if ($sendNotifications == true)
                {
                    // Invia la notifica all'utente
                    $notification = new Notification();
                    $notification->user_id = $row->user_id;
                    $notification->sender_user_id = 0;
                    $notification->message = sprintf(
                        'We\'ve changed the resource url %s to %s, it seems more valuable',
                        $row->name,
                        $updateUrl);
                    $notification->readed = false;
                    $notification->save();
                }
            }
        }
    }
}