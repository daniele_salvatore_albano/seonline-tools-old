<?php namespace Acme\Tasks\LinkBuilding;

class UpdateAllResourcesStats
{
    public function fire($job, $data)
    {
        $resources = Resource::where()


        $urlParts = parse_url(Input::get('url'));

        $url = $urlParts['scheme'] . '://' . $urlParts['host'] . (isset($urlParts['path']) ? $urlParts['path'] : '');

        $urlHostParts = explode('.', $urlParts['host']);
        $generateUrl2 = false;

        if (count($urlHostParts) == 3 && strtolower($urlHostParts[0]) == 'www')
        {
            array_shift($urlHostParts);
            $generateUrl2 = true;
        }
        else if (count($urlHostParts) == 2)
        {
            array_unshift($urlHostParts, 'www');
            $generateUrl2 = true;
        }

        $urls = array($url);

        if ($generateUrl2)
        {
            $urlParts['host'] = implode('.', $urlHostParts);

            $urls[] = $urlParts['scheme'] . '://' . $urlParts['host'] . (isset($urlParts['path']) ? $urlParts['path'] : '');
        }
    }
}