<?php

$__env->__body_class = isset($__env->__body_class)
	? (isset($__env->__body_class_add)
			? array_merge(array('contrast-green'), $__env->__body_class) : $__env->__body_class)
	: array('contrast-green');

?>

<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        
		<title>
		@section('title')
		Seo Tools
		@show
		</title>

		@section('css')
	    <link href="{{ URL::asset('stylesheets/bootstrap/bootstrap.css') }}" media="all" rel="stylesheet" type="text/css" />
	    <link href="{{ URL::asset('stylesheets/light-theme.css') }}" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
	    <link href="{{ URL::asset('stylesheets/theme-colors.css') }}" media="all" rel="stylesheet" type="text/css" />
	    <!--[if lt IE 9]>
	    <script src="{{ URL::asset('javascripts/ie/html5shiv.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/ie/respond.min.js') }}" type="text/javascript"></script>
	    <![endif]-->
		@show

		@section('js-before')
		<script type="text/javascript">
		function url(url)
		{
			return '{{ url() }}' + url;
		}

		function asset(url)
		{
			return '{{ URL::asset('') }}' + url;
		}
		</script>
		@show
	</head>
	<body class="{{ implode(' ', $__env->__body_class) }}">
        <div id="fb-root"></div>
		
        @yield('header')

        @section('content-container')
		@yield('content')
		@show
		
        @yield('footer')

		@section('js-after')
	    <script src="{{ URL::asset('javascripts/jquery/jquery.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/jquery/jquery.mobile.custom.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/jquery/jquery-migrate.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/jquery/jquery-ui.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/bootstrap/bootstrap.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/plugins/modernizr/modernizr.min.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/plugins/retina/retina.js') }}" type="text/javascript"></script>
	    <script src="{{ URL::asset('javascripts/theme.js') }}" type="text/javascript"></script>
		@show
	</body>
</html>