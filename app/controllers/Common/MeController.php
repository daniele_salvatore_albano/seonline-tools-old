<?php namespace Common;

use Auth;
use Input;
use Redirect;
use Request;
use View;
use BaseController;

class MeController extends BaseController {

    public function profile()
    {
        $this->layout->content = View::make('common/user/profile');
    }

    public function settings()
    {
        $this->layout->content = View::make('common/user/settings');
    }

    public function billing()
    {
        $this->layout->content = View::make('common/user/billing');
    }

    public function notifications()
    {
        $this->layout->content = View::make('common/user/notifications');
    }

    protected function getLayoutPath()
    {
    	if (parent::getLayoutPath() == false)
    	{
    		return false;
    	}

    	return $this->viewPath('layouts/backend');
    }
}