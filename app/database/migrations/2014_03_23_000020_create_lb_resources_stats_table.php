<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbResourcesStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_resources_stats', function($t)
		{
			$t->string('url');
			$t->integer('last_update')->unsigned();
			$t->tinyInteger('pagerank')->unsigned()->nullable();
			$t->decimal('mozrank', 5, 2)->unsigned()->nullable();
			$t->decimal('mozrank_subdomain', 5, 2)->unsigned()->nullable();
			$t->tinyInteger('page_authority')->unsigned()->nullable();
			$t->tinyInteger('domain_authority')->unsigned()->nullable();

			$t->unique(array('url', 'last_update'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_resources_stats');
	}

}
