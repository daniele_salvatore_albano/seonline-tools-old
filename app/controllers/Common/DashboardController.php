<?php namespace Common;

use Auth;
use Input;
use Redirect;
use Request;
use View;
use BaseController;

class DashboardController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('common/dashboard/index');
	}

    protected function getLayoutPath()
    {
    	if (parent::getLayoutPath() == false)
    	{
    		return false;
    	}

    	return $this->viewPath('layouts/backend');
    }
}