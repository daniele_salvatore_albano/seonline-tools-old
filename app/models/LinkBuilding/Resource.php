<?php namespace LinkBuilding;

use Eloquent;
use Validator;

class Resource extends Eloquent {

	protected $table = 'lb_resources';
	protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function stats()
    {
        return $this->hasMany('User', 'url', 'url');
    }

	public static function validate($input)
	{
    	$rules = array
    	(
            'name'          => array('required', 'min:4'),
            'url'           => array('required', 'url'),
            'type'          => array('min:3'),
            'registration'  => array('required', 'in:0,1'),
    		'enabled'       => array('required', 'in:0,1'),
		);

		$messages = array();

    	$validator = Validator::make($input, $rules, $messages);

    	if ($validator->fails())
    	{
    		return $validator;
    	}
	}

}