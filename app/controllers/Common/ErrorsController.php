<?php namespace Common;

use View;
use BaseController;

class ErrorsController extends BaseController {

	public function render($code)
	{
		$this->layout->content = View::make('common/errors/' . $code);
	}

    protected function getLayoutPath()
    {
    	return $this->viewPath('layouts/errors');
    }

}