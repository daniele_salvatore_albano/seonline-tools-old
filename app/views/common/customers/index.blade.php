<?php $counter = 0; ?>

@section('page-breadcrumb')
<li class="active">
	<h1>
        <a href="{{ URL::route('customers_list') }}">
            <i class="icon-user"></i>
            <span>Customers</span>
        </a>
    </h1>
</li>
@stop

@section('page-content')
<div class="row">
    <div class="col-sm-12">

        @if (Session::has('status'))
        <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <i class="icon-remove-sign"></i>
        {{ Session::get('status') }}
        </div>                            
        @endif

        <div class="box bordered-box green-border" style="margin-bottom:0;">
            <div class="box-header green-background">
                <div class="title">Customer List</div>
            </div>

            <div class="box-content box-no-padding">
                <div class="responsive-table">
                    <div class="scrollable-area">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <form class="row datatables-top">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>
                                        <select size="1" name="rows" onchange="this.form.submit();">
                                            <option value="10" {{ Input::get('rows') == '10' ? 'selected="selected"' : '' }}>10</option>
                                            <option value="25" {{ Input::get('rows') == '25' ? 'selected="selected"' : '' }}>25</option>
                                            <option value="50" {{ Input::get('rows') == '50' ? 'selected="selected"' : '' }}>50</option>
                                            <option value="100" {{ Input::get('rows') == '100' ? 'selected="selected"' : '' }}>100</option>
                                        </select>
                                        records per page
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <div class="dataTables_filter">
                                    <input type="text" class="form-control input-sm" placeholder="Search" name="search" value="{{{ Input::get('search') }}}" style="width: 200px;">
                                    <button class="btn btn-sm"><i class="icon-search"></i></button>
                                    &nbsp;
                                    <a class="btn btn-sm" href="{{ URL::route('customers_add') }}">
                                        <i class="icon-plus"></i>
                                        &nbsp;
                                        <span>Add Customer</span>
                                    </a>
                                </div>
                            </div>
                        </form>
                        <table class="data-table table table-hover table-striped dataTable" style="margin-bottom:0;">
                            <thead>
                                <tr role="row">
                                    <th role="columnheader">Name</th>
                                    <th role="columnheader">E-mail</th>
                                    <th role="columnheader" style="width: 174px;"></th>
                                </tr>
                            </thead>
                            <tbody role="alert">
                                @if ($customers->getTotal() == 0)
                                <tr class="odd">
                                    <td colspan="3">
                                        <div class="text-center">No customers founded</div>
                                    </td>
                                </tr>
                                @else
                                @foreach($customers as $customer)
                                <tr class="{{ $counter++ % 2 == 0 ? 'odd' : 'even' }} {{ $customer->enabled ? '' : 'warning' }}">
                                    <td class="">{{{ $customer->name }}}</td>
                                    <td class="">{{{ $customer->email }}}</td>
                                    <td class="">
                                        <div class="text-right">
                                            <a class="text-success" href="{{ URL::route('customers_edit', array('id' => $customer->id)) }}">
                                                <i class="icon-edit icon-large"></i>
                                            </a>
                                            &nbsp;
                                            @if ($customer->enabled)
                                            <a class="text-danger" href="{{ URL::route('customers_disable', array('id' => $customer->id)) }}">
                                                <i class="icon-lightbulb icon-large"></i>
                                            </a>
                                            @else
                                            <a class="text-success" href="{{ URL::route('customers_enable', array('id' => $customer->id)) }}">
                                                <i class="icon-lightbulb icon-large"></i>
                                            </a>
                                            @endif
                                            &nbsp;
                                            <a class="text-danger" href="{{ URL::route('customers_delete', array('id' => $customer->id)) }}">
                                                <i class="icon-trash icon-large"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        @if ($customers->getTotal() > $customers->count())
                        <div class="row datatables-bottom">
                            <div class="col-sm-6">
                                <div class="dataTables_info">Showing {{ $customers->getFrom() }} to {{ $customers->getTo() }} of {{ $customers->getTotal() }} entries</div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <div class="dataTables_paginate paging_bootstrap">
                                    <?php echo $customers->appends(array('rows' => Input::get('rows'), 'search' => Input::get('search')))->links(); ?>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop