<?php $counter = 0; ?>

@section('page-breadcrumb')
<li class="active">
    <h1>
        <a href="{{ URL::route('linkbuilding_resources_list') }}">
            <i class="icon-download-alt"></i>
            <span>Resources</span>
        </a>
    </h1>
</li>
@stop

@section('page-content')
<div class="row">
    <div class="col-sm-12">

        @if (Session::has('status'))
        <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <i class="icon-remove-sign"></i>
        {{ Session::get('status') }}
        </div>                            
        @endif

        <div class="box bordered-box green-border" style="margin-bottom:0;">
            <div class="box-header green-background">
                <div class="title">Resource List</div>
            </div>

            <div class="box-content box-no-padding">
                <div class="responsive-table">
                    <div class="scrollable-area">
                        <div class="dataTables_wrapper form-inline" role="grid">
                            <form class="row datatables-top">
                                <div class="col-sm-6">
                                    <div class="dataTables_length">
                                        <label>
                                            <select size="1" name="rows" onchange="this.form.submit();">
                                                <option value="10" {{ Input::get('rows') == '10' ? 'selected="selected"' : '' }}>10</option>
                                                <option value="25" {{ Input::get('rows') == '25' ? 'selected="selected"' : '' }}>25</option>
                                                <option value="50" {{ Input::get('rows') == '50' ? 'selected="selected"' : '' }}>50</option>
                                                <option value="100" {{ Input::get('rows') == '100' ? 'selected="selected"' : '' }}>100</option>
                                            </select>
                                            records per page
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <div class="dataTables_filter">
                                        <input type="text" class="form-control input-sm" placeholder="Search" name="search" value="{{{ Input::get('search') }}}" style="width: 200px;">
                                        <button class="btn btn-sm"><i class="icon-search"></i></button>
                                        &nbsp;
                                        <a class="btn btn-sm" href="{{ URL::route('linkbuilding_resources_add') }}">
                                            <i class="icon-plus"></i>
                                            &nbsp;
                                            <span>Add Resource</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <table class="data-table table table-hover table-striped dataTable" style="margin-bottom:0;">
                                <thead>
                                    <tr role="row">
                                        <th role="columnheader">Name</th>
                                        <th role="columnheader">Url</th>
                                        <th role="columnheader">Type</th>
                                        <th role="columnheader">Registration</th>
                                        <th role="columnheader">Page Rank</th>
                                        <th role="columnheader">Mozrank (Subdomain)</th>
                                        <th role="columnheader">Page Authority</th>
                                        <th role="columnheader">Domain Authority</th>
                                        <th role="columnheader">Last Update</th>
                                        <th role="columnheader" style="width: 174px;"></th>
                                    </tr>
                                </thead>
                                <tbody role="alert">
                                    @if ($resources->getTotal() == 0)
                                    <tr class="odd">
                                        <td colspan="99">
                                            <div class="text-center">No resources founded</div>
                                        </td>
                                    </tr>
                                    @else
                                    @foreach($resources as $resource)
                                    <tr class="{{ $counter++ % 2 == 0 ? 'odd' : 'even' }} {{ $resource->enabled ? '' : 'warning' }}">
                                        <td class="">{{{ $resource->name }}}</td>
                                        <td class="">{{{ $resource->url }}}</td>
                                        <td class="">{{{ $resource->type }}}</td>
                                        <td class="">
                                            @if($resource->require_registration)
                                            <span class="label label-important">Required</span>
                                            @else
                                            <span class="label label-success">Not Required</span>
                                            @endif
                                        </td>
                                        <td class="">{{{ $resource->last_update ? $resource->pagerank : '??' }}}</td>
                                        <td class="">{{{ $resource->last_update ? $resource->mozrank : '??' }}} ({{{ $resource->last_update ? $resource->mozrank_subdomain : '??' }}})</td>
                                        <td class="">{{{ $resource->last_update ? $resource->page_authority : '??' }}}</td>
                                        <td class="">{{{ $resource->last_update ? $resource->domain_authority : '??' }}}</td>
                                        <td class="">{{{ $resource->last_update ? date('d/m/Y', $resource->last_update) : '??' }}}</td>
                                        <td class="">
                                            <div class="text-right">
                                                <a class="text-success" href="{{ URL::route('linkbuilding_resources_edit', array('id' => $resource->id)) }}">
                                                    <i class="icon-edit icon-large"></i>
                                                </a>
                                                &nbsp;
                                                @if ($resource->enabled)
                                                <a class="text-danger" href="{{ URL::route('linkbuilding_resources_disable', array('id' => $resource->id)) }}">
                                                    <i class="icon-lightbulb icon-large"></i>
                                                </a>
                                                @else
                                                <a class="text-success" href="{{ URL::route('linkbuilding_resources_enable', array('id' => $resource->id)) }}">
                                                    <i class="icon-lightbulb icon-large"></i>
                                                </a>
                                                @endif
                                                &nbsp;
                                                <a class="text-danger" href="{{ URL::route('linkbuilding_resources_delete', array('id' => $resource->id)) }}">
                                                    <i class="icon-trash icon-large"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @if ($resources->getTotal() > $resources->count())
                            <div class="row datatables-bottom">
                                <div class="col-sm-6">
                                    <div class="dataTables_info">Showing {{ $resources->getFrom() }} to {{ $resources->getTo() }} of {{ $resources->getTotal() }} entries</div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <?php echo $resources->appends(array('rows' => Input::get('rows'), 'search' => Input::get('search')))->links(); ?>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop