<?php

Route::pattern('id', '[0-9]+');
Route::pattern('id2', '[0-9]+');

// Common
Route::group(array('before' => 'auth', 'prefix' => 'common'), function()
{
    // Me
    Route::group(array('prefix' => 'me'), function()
    {
        Route::get('/', function() { return Redirect::route('me_profile'); });
        Route::match(array('GET', 'POST'), '/profile', array('as' => 'me_profile', 'uses' => 'Common\MeController@profile'));
        Route::match(array('GET', 'POST'), '/settings', array('as' => 'me_settings', 'uses' => 'Common\MeController@settings'));
        Route::match(array('GET', 'POST'), '/notifications/{id?}', array('as' => 'me_notifications', 'uses' => 'Common\MeController@notifications'));
        Route::match(array('GET', 'POST'), '/billing', array('as' => 'me_billing', 'uses' => 'Common\MeController@billing'));
    });

    // Customers
    Route::group(array('prefix' => 'customers'), function()
    {
        Route::match(array('GET', 'POST'), '/', array('as' => 'customers_list', 'uses' => 'Common\CustomersController@index'));
        Route::match(array('GET', 'POST'), '/add', array('as' => 'customers_add', 'uses' => 'Common\CustomersController@add'));
        Route::match(array('GET', 'POST'), '/{id}', array('as' => 'customers_view', 'uses' => 'Common\CustomersController@view'));
        Route::match(array('GET', 'POST'), '/{id}/edit', array('as' => 'customers_edit', 'uses' => 'Common\CustomersController@edit'));
        Route::match(array('GET', 'POST'), '/{id}/delete', array('as' => 'customers_delete', 'uses' => 'Common\CustomersController@delete'));
        Route::match(array('GET', 'POST'), '/{id}/enable', array('as' => 'customers_enable', 'uses' => 'Common\CustomersController@enable'));
        Route::match(array('GET', 'POST'), '/{id}/disable', array('as' => 'customers_disable', 'uses' => 'Common\CustomersController@disable'));
    });

    // Projects
    Route::group(array('prefix' => 'projects'), function()
    {
        Route::match(array('GET', 'POST'), '/', array('as' => 'projects_list', 'uses' => 'Common\ProjectsController@index'));
        Route::match(array('GET', 'POST'), '/add', array('as' => 'projects_add', 'uses' => 'Common\ProjectsController@add'));
        Route::match(array('GET', 'POST'), '/{id}', array('as' => 'projects_view', 'uses' => 'Common\ProjectsController@view'));
        Route::match(array('GET', 'POST'), '/{id}/edit', array('as' => 'projects_edit', 'uses' => 'Common\ProjectsController@edit'));
        Route::match(array('GET', 'POST'), '/{id}/delete', array('as' => 'projects_delete', 'uses' => 'Common\ProjectsController@delete'));
        Route::match(array('GET', 'POST'), '/{id}/enable', array('as' => 'projects_enable', 'uses' => 'Common\ProjectsController@enable'));
        Route::match(array('GET', 'POST'), '/{id}/disable', array('as' => 'projects_disable', 'uses' => 'Common\ProjectsController@disable'));
    });
    
    // Dashboard
    Route::group(array('prefix' => 'dashboard'), function()
    {
        Route::get('/', array('as' => 'dashboard', 'uses' => 'Common\DashboardController@index'));
    });
});

// Link Building
Route::group(array('before' => 'auth', 'prefix' => 'linkbuilding'), function()
{
    // Resources
    Route::group(array('prefix' => 'resources'), function()
    {
        Route::match(array('GET', 'POST'), '/', array('as' => 'linkbuilding_resources_list', 'uses' => 'LinkBuilding\ResourcesController@index'));
        Route::match(array('GET', 'POST'), '/add', array('as' => 'linkbuilding_resources_add', 'uses' => 'LinkBuilding\ResourcesController@add'));
        Route::match(array('GET', 'POST'), '/{id}', array('as' => 'linkbuilding_resources_view', 'uses' => 'LinkBuilding\ResourcesController@view'));
        Route::match(array('GET', 'POST'), '/{id}/edit', array('as' => 'linkbuilding_resources_edit', 'uses' => 'LinkBuilding\ResourcesController@edit'));
        Route::match(array('GET', 'POST'), '/{id}/delete', array('as' => 'linkbuilding_resources_delete', 'uses' => 'LinkBuilding\ResourcesController@delete'));
        Route::match(array('GET', 'POST'), '/{id}/enable', array('as' => 'linkbuilding_resources_enable', 'uses' => 'LinkBuilding\ResourcesController@enable'));
        Route::match(array('GET', 'POST'), '/{id}/disable', array('as' => 'linkbuilding_resources_disable', 'uses' => 'LinkBuilding\ResourcesController@disable'));
    });

    // Projects
    Route::group(array('prefix' => 'projects'), function()
    {
        Route::match(array('GET', 'POST'), '/', array('as' => 'linkbuilding_projects_list', 'uses' => 'LinkBuilding\ProjectsController@index'));
        Route::match(array('GET', 'POST'), '/add', array('as' => 'linkbuilding_projects_add', 'uses' => 'LinkBuilding\ProjectsController@add'));
        Route::match(array('GET', 'POST'), '/{id}', array('as' => 'linkbuilding_projects_view', 'uses' => 'LinkBuilding\ProjectsController@view'));
        Route::match(array('GET', 'POST'), '/{id}/edit', array('as' => 'linkbuilding_projects_edit', 'uses' => 'LinkBuilding\ProjectsController@edit'));
        Route::match(array('GET', 'POST'), '/{id}/delete', array('as' => 'linkbuilding_projects_delete', 'uses' => 'LinkBuilding\ProjectsController@delete'));
        Route::match(array('GET', 'POST'), '/{id}/enable', array('as' => 'linkbuilding_projects_enable', 'uses' => 'LinkBuilding\ProjectsController@enable'));
        Route::match(array('GET', 'POST'), '/{id}/disable', array('as' => 'linkbuilding_projects_disable', 'uses' => 'LinkBuilding\ProjectsController@disable'));

        Route::group(array('prefix' => '/{id}/resources'), function()
        {
            Route::match(array('GET', 'POST'), '/', array('as' => 'linkbuilding_projects_resources_list', 'uses' => 'LinkBuilding\ProjectsResourcesController@index'));
            Route::match(array('GET', 'POST'), '/add', array('as' => 'linkbuilding_projects_resources_add', 'uses' => 'LinkBuilding\ProjectsResourcesController@add'));
            Route::match(array('GET', 'POST'), '/{id2}', array('as' => 'linkbuilding_projects_resources_view', 'uses' => 'LinkBuilding\ProjectsResourcesController@view'));
            Route::match(array('GET', 'POST'), '/{id2}/edit', array('as' => 'linkbuilding_projects_resources_edit', 'uses' => 'LinkBuilding\ProjectsResourcesController@edit'));
            Route::match(array('GET', 'POST'), '/{id2}/delete', array('as' => 'linkbuilding_projects_resources_delete', 'uses' => 'LinkBuilding\ProjectsResourcesController@delete'));
            Route::match(array('GET', 'POST'), '/{id2}/enable', array('as' => 'linkbuilding_projects_resources_enable', 'uses' => 'LinkBuilding\ProjectsResourcesController@enable'));
            Route::match(array('GET', 'POST'), '/{id2}/disable', array('as' => 'linkbuilding_projects_resources_disable', 'uses' => 'LinkBuilding\ProjectsResourcesController@disable'));
        });
    });
});

Route::get('/login', array('as' => 'login', 'uses' => 'Common\AuthController@login'));
Route::get('/login/google', array('as' => 'login-using-google', 'uses' => 'Common\AuthController@loginGoogle'));
Route::get('/login/facebook', array('as' => 'login-using-facebook', 'uses' => 'Common\AuthController@loginFacebook'));
Route::get('/login/twitter', array('as' => 'login-using-twitter', 'uses' => 'Common\AuthController@loginTwitter'));
Route::get('/login/linkedin', array('as' => 'login-using-linkedin', 'uses' => 'Common\AuthController@loginLinkedin'));
Route::post('/login', array('before' => 'csrf', 'uses' => 'Common\AuthController@login'));
Route::get('/logout', array('before' => 'auth', 'as' => 'logout', 'uses' => 'Common\AuthController@logout'));

Route::get('/register', array('as' => 'register', 'uses' => 'Common\AuthController@register'));
Route::post('/register', array('before' => 'csrf', 'uses' => 'Common\AuthController@register'));

Route::get('/recovery-password', function() { return Redirect::route('recovery-password'); });

Route::get('/recovery-password/remind', array('as' => 'recovery-password', 'uses' => 'Common\RecoveryPasswordController@remind'));
Route::post('/recovery-password/remind', array('before' => 'csrf', 'uses' => 'Common\RecoveryPasswordController@remind'));

Route::get('/recovery-password/reset', array('as' => 'recovery-password-reset', 'uses' => 'Common\RecoveryPasswordController@reset'));
Route::post('/recovery-password/reset', array('before' => 'csrf', 'uses' => 'Common\RecoveryPasswordController@reset'));

Route::get('/error/{code}', array('uses' => 'Common\ErrorsController@render'));
Route::match(array('GET', 'POST'), '/{name}', array('uses' => 'Common\StaticPagesController@render'))->where('name', '[a-z0-9\-\/]+');

Route::get('/', function() { return Redirect::to('/index'); });