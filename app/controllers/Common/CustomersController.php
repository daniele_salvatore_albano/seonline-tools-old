<?php namespace Common;

use Auth;
use Input;
use Redirect;
use Request;
use View;
use BaseController;

use Customer;

class CustomersController extends BaseController {

    public function index()
    {
        // Instanzia il modello
        $customerModel = new Customer();

        // Instanzia la query
        $customersQuery = $customerModel->newQuery();

        // Filtra per id utente
        $customersQuery->where('user_id', $this->me->id);

        // Se è stato passato il filtro di ricerca lo applica
        if (Input::has('search'))
        {
            $customersQuery->where('name', 'LIKE', '%' . Input::get('search') . '%');
            $customersQuery->orWhere('email', 'LIKE', '%' . Input::get('search') . '%');
        }

        // Applica l'ordinamento
        $customersQuery->orderBy('name', 'asc');

        // Esegue la paginazione
        $customers = $customersQuery->paginate(Input::get('rows', '10'));

        // Renderizza la view
        $this->layout->content = View::make('common/customers/index')->with('customers', $customers);
    }

    public function add()
    {
        if ($result = $this->handleAddOrEdit()) return $result;

        $this->layout->content = View::make('common/customers/edit')->with('customer', null);
    }

    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        if ($result = $this->handleAddOrEdit($customer)) return $result;

        $this->layout->content = View::make('common/customers/edit')->with('customer', $customer);
    }

    public function delete($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return Redirect::route('customers_list')->with('status', 'Customer successfully deleted');
    }

    public function disable($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->enabled = false;
        $customer->save();

        return Redirect::route('customers_list')->with('status', 'Customer successfully disabled');
    }

    public function enable($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->enabled = true;
        $customer->save();

        return Redirect::route('customers_list')->with('status', 'Customer successfully enabled');
    }

    private function handleAddOrEdit($customer = null)
    {
        if (Request::isMethod('post'))
        {
            if ($validator = Customer::validate(Input::all()))
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            if ($customer == null)
            {
                $customer = new Customer();
                $customer->user_id = $this->me->id;
            }

            $customer->name = Input::get('name');
            $customer->email = Input::get('email');
            $customer->notes = Input::get('notes');
            $customer->enabled = Input::get('enabled');
            $customer->save();

            return Redirect::route('customers_edit', array('id' => $customer->id))->with('status', 'Customer successfully saved');
        }        
    }

    protected function getLayoutPath()
    {
    	if (parent::getLayoutPath() == false)
    	{
    		return false;
    	}

    	return $this->viewPath('layouts/backend');
    }
}