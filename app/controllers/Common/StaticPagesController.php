<?php namespace Common;

use Request;
use BaseController;

class StaticPagesController extends BaseController
{
	public function render($name)
	{
		// Pulisce il nome della pagina per mapparlo al nome di un file valido su disco
        $pagePath = $name;
		$pagePath = preg_replace('#\-+#', '-', $pagePath);	
        $pagePath = str_replace('/', '_', $pagePath);

		// Verifica se esiste il metodo pre{METHOD}{NAME} (es. beforeGetRules o afterPostContact)
		if (($preView = $this->callPrePostRelatedPageMethod($name, 'before')))
		{
			return $preView;
		}

		// Verifica se esiste la pagina statica
		$view = $this->view('common/static-pages/' . $pagePath);

		if (($postView = $this->callPrePostRelatedPageMethod($name, 'after', array($view))))
		{
			return $postView;
		}

		return $view;
	}

	protected function callPrePostRelatedPageMethod($name, $type)
	{
        // Acquisisce il metodo
        $requestMethod = Request::getMethod();

        // Pulisce il nome
        $methodName = strtolower($requestMethod . ' ' . $name);
        $methodName = preg_replace('#[^a-z0-9 ]+#', ' ', $methodName);
        $methodName = ucwords($methodName);
        $methodName = str_replace(' ', '', $methodName);

		// Verifica se il metodo esiste
		if (method_exists($this, $methodName) == true)
		{
			if (($view = call_user_func_array($this, $methodName)) != false)
			{
				return $view;
			}
		}

		return null;
	}
}