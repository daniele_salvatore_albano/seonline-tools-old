<?php namespace LinkBuilding;

use Eloquent;
use Validator;

class ResourceStats extends Eloquent {

	protected $table = 'lb_resources_stats';
	protected $softDelete = false;
    public $timestamps = false;
    
}