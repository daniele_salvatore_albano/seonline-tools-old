<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Google
		 */
		'Google' => array(
		    'client_id'     => '910402079319-a7v88u0n3jnqm4c9kc8jjvjqgsqaqh3m.apps.googleusercontent.com',
		    'client_secret' => 'g4eXnJfU9EzdzM84BdAsXG4P',
		    'scope'         => array('userinfo_email', 'userinfo_profile'),
		),

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '792180724127219',
            'client_secret' => 'b94385a10354213ef65665ffeedd96f2',
            'scope'         => array('email'),
        ),

        /**
         * Linkedin
         */
		'Linkedin' => array(
		    'client_id'     => '77iv4l7x99sjq5',
		    'client_secret' => '9pgw0J5PsNPVSuVk',
		    'scope'			=> array('r_basicprofile', 'r_emailaddress')
		), 

        /**
         * Twitter
         */
		'Twitter' => array(
		    'client_id'     => '0gsQolr1Tvc2xtuFEwg8Xg',
		    'client_secret' => 'w03TJGcXaePm7tXesvMPrdYorvR6pF6dTd4FrdFXM'
		), 
	)

);