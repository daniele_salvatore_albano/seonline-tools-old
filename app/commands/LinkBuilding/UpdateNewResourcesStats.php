<?php namespace LinkBuilding;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateNewResourcesStats extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'linkbuilding:updatenewresourcesstats';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update resources stats (page rank, mozrank, domain authority, page authority, etc.)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\Acme\Tasks\LinkBuilding\UpdateNewResourcesStats::fire();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
