<?php namespace Common;

use Auth;
use Hash;
use Input;
use OAuth;
use Redirect;
use Request;
use Str;
use View;
use URL;

use User;

use BaseController;

class AuthController extends BaseController {

	public function login()
	{
		if ($result = $this->loginCommon()) return $result;

		if (Request::isMethod('post'))
		{
			if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')), Input::has('remember_me')))
			{
			    return Redirect::intended(URL::route('dashboard'));
			}

			return Redirect::back()->withInput()->with('error', 'Invalid credentials or account suspended');
		}

		$this->layout->content = View::make('common/auth/login');
	}

	public function loginGoogle()
	{
		if ($result = $this->loginCommon()) return $result;

	    $oauth = OAuth::consumer('Google');

	    if (Input::has('code'))
	    {
	        $token = $oauth->requestAccessToken(Input::get('code'));

	        $me = json_decode($oauth->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

	        if (($user = User::where('oauth_google_id', '=', $me['id'])->first()) == null)
	        {
        		if (($user = User::where('email', '=', $me['email'])->first()) != null)
        		{
		        	return Redirect::route('login')->with('error',
		        		'An account using the same e-mail address exists, if you want to login using google please link your account');
        		}

	        	$user = new User();
	        	$user->email = $me['email'];
	        	$user->password = Hash::make(Str::random(10));
	        	$user->display_name = $me['name'];
	        	$user->first_name = $me['given_name'];
	        	$user->last_name = $me['family_name'];
	        	$user->avatar_url = $me['picture'];
	        	$user->oauth_google_id = $me['id'];
	        	$user->validated = 1;
	        	$user->completed = 1;
	        	$user->enabled = 1;
        		$user->save();
	        }

	        Auth::login($user);

	        return Redirect::route('dashboard');
	    }

        $url = $oauth->getAuthorizationUri();

        return Redirect::to((string)$url);
	}

	public function loginFacebook()
	{
		if ($result = $this->loginCommon()) return $result;

	    $oauth = OAuth::consumer('Facebook');

	    if (Input::has('code'))
	    {
	        $token = $oauth->requestAccessToken(Input::get('code'));

	        $me = json_decode($oauth->request('/me'), true);

	        if (($user = User::where('oauth_facebook_id', '=', $me['id'])->first()) == null)
	        {
        		if (($user = User::where('email', '=', $me['email'])->first()) != null)
        		{
		        	return Redirect::route('login')->with('error',
		        		'An account using the same e-mail address exists, if you want to login using facebook please link your account');
        		}

	        	$user = new User();
	        	$user->email = $me['email'];
	        	$user->password = Hash::make(Str::random(10));
	        	$user->display_name = $me['name'];
	        	$user->first_name = $me['first_name'];
	        	$user->last_name = $me['last_name'];
	        	$user->avatar_url = 'http://graph.facebook.com/' . $me['id'] . '/picture?width=150';
	        	$user->oauth_facebook_id = $me['id'];
	        	$user->validated = 1;
	        	$user->completed = 1;
	        	$user->enabled = 1;
        		$user->save();
	        }

	        Auth::login($user);

	        return Redirect::route('dashboard');
	    }

        $url = $oauth->getAuthorizationUri();

        return Redirect::to((string)$url);
	}

	public function loginTwitter()
	{
		if ($result = $this->loginCommon()) return $result;

		$oauth = OAuth::consumer('Twitter');

		if (Input::has('oauth_token'))
		{
			$token = $oauth->getStorage()->retrieveAccessToken('Twitter');

            $oauth->requestAccessToken(Input::get('oauth_token'), Input::get('oauth_verifier'), $token->getRequestTokenSecret());

            $me = json_decode($oauth->request('account/verify_credentials.json?include_entities=false&skip_status=true'));

	        if (($user = User::where('oauth_twitter_id', '=', $me['id'])->first()) == null)
	        {
	        	$user = new User();
	        	$user->email = '';
	        	$user->password = '';
	        	$user->display_name = $me['name'];
	        	$user->avatar_url = $me['profile_image_url_https'];
	        	$user->oauth_twitter_id = $me['id'];
	        	$user->validated = 1;
	        	$user->completed = 0;
	        	$user->enabled = 1;
        		$user->save();
	        }

	        Auth::login($user);

	        echo 'TODO - deve completare la registrazione';
	        die();

	        return Redirect::route('register');
    	}

		$token = $oauth->requestRequestToken();
		$url = $oauth->getAuthorizationUri(['oauth_token' => $token->getRequestToken()]);

        return Redirect::to((string)$url);
	}

	public function loginLinkedin()
	{
		if ($result = $this->loginCommon()) return $result;

        $oauth = OAuth::consumer('Linkedin');

        if (Input::has('code'))
        {
            $token = $oauth->requestAccessToken(Input::get('code'));

            $me = json_decode($oauth->request('/people/~:(id,first-name,last-name,formatted-name,picture-url,email-address)?format=json'), true);

	        if (($user = User::where('oauth_linkedin_id', '=', $me['id'])->first()) == null)
	        {
        		if (($user = User::where('email', '=', $me['emailAddress'])->first()) != null)
        		{
		        	return Redirect::route('login')->with('error',
		        		'An account using the same e-mail address exists, if you want to login using facebook please link your account');
        		}

	        	$user = new User();
	        	$user->email = $me['emailAddress'];
	        	$user->password = Hash::make(Str::random(10));
	        	$user->display_name = $me['formattedName'];
	        	$user->first_name = $me['firstName'];
	        	$user->last_name = $me['lastName'];
	        	$user->avatar_url = $me['pictureUrl'];
	        	$user->oauth_linkedin_id = $me['id'];
	        	$user->validated = 1;
	        	$user->completed = 1;
	        	$user->enabled = 1;
        		$user->save();
	        }

	        Auth::login($user);

	        return Redirect::route('dashboard');
        }

        $url = $oauth->getAuthorizationUri(array('state'=>'BCEEFWF45453sdffef424'));

        return Redirect::to((string)$url);
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::to('/');
	}

	public function register()
	{
		if ($user = Auth::user())
		{
			if ($user->completed == true && $user->validated == true)
			{
				return Redirect::route('dashboard');
			}
		}

		// Deve essere gestita la casistica dell'utente registrato ma con il
		// profilo non completo (twitter) oppure la casistica con la registrazione
		// effettuata manualmente ma non validata

		if (Request::isMethod('post'))
		{
			if ($errors = User::validate(Input::all(), $user))
			{
				return Redirect::back()->withInput()->with('errors', $errors);
			}

        	$user = new User();
        	$user->email = Input::get('email');
        	$user->password = Hash::make(Input::get('password'));
        	$user->display_name = Input::get('first_name') . ' ' . Input::get('last_name');
        	$user->first_name = Input::get('first_name');
        	$user->last_name = Input::get('last_name');
        	$user->validated = 0;
        	$user->validation_token = Str::random(30);
        	$user->completed = 1;
        	$user->enabled = 1;
    		$user->save();

			return Redirect::route('login')
				->withInput()
				->with('status', 'Registration successful, an activation email has been sent to your email address! Once activated you will be able to use our tools!');
		}

		$this->layout->content = View::make('common/auth/register')->with('registered', false);
	}

	private function loginCommon()
	{
		if (Auth::check())
		{
			return Redirect::route('dashboard');
		}
	}
}