<?php

$counter = 0;

$resourceTypes = LinkBuilding\Resource::where('enabled', '1')->lists('type');
$resourceTypes[] = 'article marketing';
$resourceTypes[] = 'directory';
$resourceTypes[] = 'guest post';
$resourceTypes[] = 'link';

$resourceTypes = array_unique($resourceTypes);
natsort($resourceTypes);

?>

@section('page-breadcrumb')
<li class="active">
    <h1>
        <a href="{{ URL::route('linkbuilding_resources_list') }}">
            <i class="icon-download-alt"></i>
            <span>Resources</span>
        </a>
    </h1>
</li>
<li class="separator"><i class="icon-angle-right"></i></li>
@if ($resource)
<li class="active">Edit</li>
@else
<li class="active">Add New</li>
@endif
@stop

@section('page-content')
<div class="row">
    <div class="col-sm-12">

        @if (Session::has('status'))
        <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <i class="icon-remove-sign"></i>
        {{ Session::get('status') }}
        </div>                            
        @endif

        <div class="box">
            <div class="box-header green-background">
                <div class="title">
                    @if ($resource)
                    <div class="icon-edit"></div>
                    Edit Resource &laquo;{{{ $resource->name }}}&raquo;
                    @else
                    <div class="icon-plus"></div>
                    Add New Resource
                    @endif
                </div>
            </div>
            <div class="box-content">
                <form class="form form-horizontal" style="margin-bottom: 0;" method="post">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-md-5">
                            <input class="form-control" placeholder="Name" type="text" id="name" name="name" value="{{{ Input::old('name', $resource ? $resource->name : '') }}}">
                            {{ implode("\r\n", $errors->get('name', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="url">Url</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon">http://</span>
                                <input class="form-control" placeholder="url" type="text" id="url" name="url" value="{{{ Input::old('url', $resource ? $resource->url : '') }}}">
                            </div>
                            {{ implode("\r\n", $errors->get('url', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="type">Type</label>
                        <div class="col-md-5">
                            <input class="form-control" placeholder="Type" type="text" id="type" name="type" value="{{{ Input::old('type', $resource ? $resource->type : '') }}}">
                            {{ implode("\r\n", $errors->get('type', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('registration') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label">Registration</label>
                        <div class="col-md-10">
                            <label class="radio radio-inline">
                                <input type="radio" name="registration" value="1" {{ Input::old('registration', $resource ? $resource->require_registration : '0') == '1' ? 'checked' : '' }}>
                                Required
                            </label>
                            <label class="radio radio-inline">
                                <input type="radio" name="registration" value="0" {{ Input::old('registration', $resource ? $resource->require_registration : '0') == '0' ? 'checked' : '' }}>
                                Not Required
                            </label>
                            {{ implode("\r\n", $errors->get('registration', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="notes">Notes</label>
                        <div class="col-md-5">
                            <textarea class="form-control" id="notes" name="notes" placeholder="Notes" rows="3">{{{ Input::old('Notes', $resource ? $resource->notes : '') }}}</textarea>
                            {{ implode("\r\n", $errors->get('notes', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('enabled') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label">Enabled</label>
                        <div class="col-md-10">
                            <label class="radio radio-inline">
                                <input type="radio" name="enabled" value="1" {{ Input::old('enabled', $resource ? $resource->enabled : '1') == '1' ? 'checked' : '' }}>
                                Yes
                            </label>
                            <label class="radio radio-inline">
                                <input type="radio" name="enabled" value="0" {{ Input::old('enabled', $resource ? $resource->enabled : '1') == '0' ? 'checked' : '' }}>
                                No
                            </label>
                            {{ implode("\r\n", $errors->get('enabled', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-actions form-actions-padding-sm">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <button class="btn btn-primary" type="submit">
                                    <i class="icon-save"></i>
                                    Save
                                </button>
                                <a class="btn" type="submit" href="{{ URL::route('linkbuilding_resources_list') }}">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<link href="{{ URL::asset('stylesheets/plugins/select2/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop

@section('js-after')
@parent
<script src="{{ URL::asset('javascripts/plugins/select2/select2.js') }}" type="text/javascript"></script>


<script type="text/javascript">
$(function()
{
    $("INPUT[name=type]").select2(
    {
        placeholder: "Select a resource type",
        createSearchChoice: function(term, data)
        {
            if ($(data).filter(function()
            {
                return this.text.localeCompare(term) === 0;
            }).length===0)
            {
                return { 'id': term, 'text': term };
            }
        },
        data:
        [
            @foreach($resourceTypes as $type)
            { id: '{{{ $type }}}', text: '{{{ $type }}}'},
            @endforeach
        ]
    });
})
</script>
@stop