<?php namespace Acme\Support\Seo;

class Mozscape
{
    private static $fields = array
    (
        'title'                                 => 1,
        'canonical-url'                         => 4,
        'subdomain'                             => 8,
        'root-domain'                           => 16,
        'external-equity-links'                 => 32,
        'subdomain-external-links'              => 64,
        'root-domain-external-links'            => 128,
        'equity-links'                          => 256,
        'subdomains-linking'                    => 512,
        'root-domains-linking'                  => 1024,
        'links'                                 => 2048,
        'subdomain-subdomains-linking'          => 4096,
        'root-domain-root-domains-linking'      => 8192,
        'mozrank'                               => 16384,
        'subdomain-mozrank'                     => 32768,
        'root-domain-mozrank'                   => 65536,
        'moztrust'                              => 131072,
        'subdomain-moztrust'                    => 262144,
        'root-domain-moztrust'                  => 524288,
        'external-mozrank'                      => 1048576,
        'subdomain-external-link-equity'        => 2097152,
        'root-domain-external-link-equity'      => 4194304,
        'subdomain-link-equity'                 => 8388608,
        'root-domain-link-equity'               => 16777216,
        'http-status-code'                      => 536870912,
        'links-to-subdomain'                    => 4294967296,
        'links-to-root-domain'                  => 8589934592,
        'root-domains-linking-to-subdomain'     => 17179869184,
        'page-authority'                        => 34359738368,
        'domain-authority'                      => 68719476736,
        'time-last-crawled'                     => 144115188075855872
    );
    
    private static $keyToField = array
    (
        'ut'    => 'title',                                
        'uu'    => 'canonical-url',                        
        'ufq'   => 'subdomain',                            
        'upl'   => 'root-domain',                          
        'ueid'  => 'external-equity-links',                
        'feid'  => 'subdomain-external-links',             
        'peid'  => 'root-domain-external-links',           
        'ujid'  => 'equity-links',                         
        'uifq'  => 'subdomains-linking',                   
        'uipl'  => 'root-domains-linking',                 
        'uid'   => 'links',                                
        'fid'   => 'subdomain-subdomains-linking',         
        'pid'   => 'root-domain-root-domains-linking',     
        'umrp'  => 'mozrank',                              
        'fmrp'  => 'subdomain-mozrank',                    
        'pmrp'  => 'root domain-mozrank',                  
        'utrp'  => 'moztrust',                             
        'ftrp'  => 'subdomain-moztrust',                   
        'ptrp'  => 'root domain-moztrust',                 
        'uemrp' => 'external-mozrank',                     
        'fejp'  => 'subdomain-external-link-equity',       
        'pejp'  => 'root-domain-external-link-equity',     
        'fjp'   => 'subdomain-link-equity',                
        'pjp'   => 'root-domain-link-equity',              
        'us'    => 'http-status-code',                     
        'fuid'  => 'links-to-subdomain',                   
        'puid'  => 'links-to-root-domain',                 
        'fipl'  => 'root-domains-linking-to-subdomain',    
        'upa'   => 'page-authority',                       
        'pda'   => 'domain-authority',                     
        'ulc'   => 'time-last-crawled'
    );
    
    public static function waitRateLimit()
    {
        sleep(\Config::get('seo.mozscape.ratelimit', 10));
    }
    /*

    public static function getPageAuthority($urls)
    {
        $data = self::getCols('34359738368', $urls);
        return $data == false ? $data : $data['upa'];
    }

    public static function getDomainAuthority($urls)
    {
        if (is_array($urls) == true)
        {
            foreach($urls as $url)
            {
                $hostnames[] = parse_url($url, PHP_URL_HOST);
            }
        }
        else
        {
            $hostnames = parse_url($urls, PHP_URL_HOST);
        }

        $data = self::getCols('68719476736', $hostnames);
        return $data == false ? $data : $data['pda'];
    }

    public static function getEquityLinkCount($urls)
    {
        $data = self::getCols('2048', $urls);
        return $data == false ? $data : $data['uid'];
    }

    public static function getLinkCount($urls)
    {
        $data = self::getCols('2048', $urls);
        return $data == false ? $data : $data['uid'];
    }

    public static function getMozRank($urls)
    {
        $data = self::getCols('16384', $urls);
        return $data == false ? $data : $data['umrp'];
    }
*/
    public static function lastUpdate()
    {
        if (\Config::get('seo.mozscape.access_id') == false || \Config::get('seo.mozscape.secret_key') == false)
        {
            throw new \Exception('Missing Mozscape AccessID and/or Secret Key');
        }

        // Make the request
        $result = self::request('http://lsapi.seomoz.com/linkscape/metadata/last_update.json?');

        return $result ? $result['last_update'] : false;
    }

    public static function nextUpdate()
    {
        if (\Config::get('seo.mozscape.access_id') == false || \Config::get('seo.mozscape.secret_key') == false)
        {
            throw new \Exception('Missing Mozscape AccessID and/or Secret Key');
        }

        // Make the request
        $result = self::request('http://lsapi.seomoz.com/linkscape/metadata/next_update.json?');

        return $result ? $result['next_update'] : false;
    }

    public static function urlMetrics($fields, $urls)
    {
        if (\Config::get('seo.mozscape.access_id') == false || \Config::get('seo.mozscape.secret_key') == false)
        {
            throw new \Exception('Missing Mozscape AccessID and/or Secret Key');
        }

        // Convert fields and urls to arrays
        $fields = is_array($fields) ? $fields : array($fields);
        $urls = is_array($urls) ? $urls : array($urls);

        // Calculate cols
        $cols = sprintf('%u', 0);
        foreach($fields as $field)
        {
            $cols += self::$fields[strtolower($field)];
        }
        $cols = number_format($cols, 0, '', '');

        // Make the request
        $result = self::request('http://lsapi.seomoz.com/linkscape/url-metrics/?Cols=' . $cols, 'POST', json_encode($urls));

        // Build the return value
        $returnValue = array();
        foreach($result as $index => $row)
        {
            $returnValue[$urls[$index]] = array();

            foreach($row as $key => $value)
            {
                if (isset(self::$keyToField[$key]) == false)
                {
                    continue;
                }
                
                $returnValue[$urls[$index]][self::$keyToField[$key]] = $value;
            }
        }

        return $returnValue;
    }

    private static function request($url, $method = 'GET', $body = null, $contentType = 'application/x-www-form-urlencoded')
    {
        $stream = null;

        // Calculate the expiration time
        $expires = time() + 300;

        // Build the signature
        $signature = base64_encode(hash_hmac(
            'sha1',
            \Config::get('seo.mozscape.access_id') . "\n" . $expires, 
            \Config::get('seo.mozscape.secret_key'),
            true));

        // Build the request url
        $apiEndpoint = $url .
            'AccessID=' . \Config::get('seo.mozscape.access_id') .
            '&' .
            'Expires=' . $expires .
            '&' .
            'Signature=' . urlencode($signature);

        if (strtolower($method) != 'get')
        {
            // Create the stream context
            $stream = stream_context_create(array
            (
                'http' => array
                (
                    'method'    => $method,
                    'header'    => 'Content-Type: ' . $contentType . '\r\n',
                    'content'   =>  $body
                )
            ));
        }

        // Request the page
        $result = file_get_contents($apiEndpoint, false, $stream);

        // Check the result
        if ($result == false || strlen(trim($result)) == 0 || ($result = json_decode($result, true)) == false)
        {
            return false;
        }

        return $result;
    }
}
