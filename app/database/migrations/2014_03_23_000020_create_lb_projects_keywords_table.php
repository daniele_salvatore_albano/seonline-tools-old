<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbProjectsKeywordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_projects_keywords', function($t)
		{
			$t->increments('id');
			$t->integer('project_id')->unsigned()->index();
			$t->string('keyword');
			$t->string('url');
			$t->boolean('enabled');
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_projects_keywords');
	}

}
