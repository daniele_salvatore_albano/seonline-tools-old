@section('page-breadcrumb')
<li class="active">
    <h1>
        <a href="{{ URL::route('customers_list') }}">
            <i class="icon-user"></i>
            <span>Customers</span>
        </a>
    </h1>
</li>
<li class="separator"><i class="icon-angle-right"></i></li>
@if ($customer)
<li class="active">Edit</li>
@else
<li class="active">Add New</li>
@endif
@stop

@section('page-content')
<div class="row">
    <div class="col-sm-12">

        @if (Session::has('status'))
        <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <i class="icon-remove-sign"></i>
        {{ Session::get('status') }}
        </div>                            
        @endif

        <div class="box">
            <div class="box-header green-background">
                <div class="title">
                    @if ($customer)
                    <div class="icon-edit"></div>
                    Edit Customer &laquo;{{{ $customer->name }}}&raquo;
                    @else
                    <div class="icon-plus"></div>
                    Add New Customer
                    @endif
                </div>
            </div>
            <div class="box-content">
                <form class="form form-horizontal" style="margin-bottom: 0;" method="post">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-md-5">
                            <input class="form-control" placeholder="Name" type="text" id="name" name="name" value="{{{ Input::old('name', $customer ? $customer->name : '') }}}">
                            {{ implode("\r\n", $errors->get('name', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="email">E-Mail</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <input class="form-control" id="email" name="email" placeholder="E-Mail" type="text" value="{{{ Input::old('email', $customer ? $customer->email : '') }}}">
                                <span class="input-group-addon">@</span>
                            </div>
                            {{ implode("\r\n", $errors->get('email', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label" for="notes">Notes</label>
                        <div class="col-md-5">
                            <textarea class="form-control" id="notes" name="notes" placeholder="Notes" rows="3">{{{ Input::old('Notes', $customer ? $customer->notes : '') }}}</textarea>
                            {{ implode("\r\n", $errors->get('notes', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('enabled') ? 'has-error' : '' }}">
                        <label class="col-md-2 control-label">Enabled</label>
                        <div class="col-md-10">
                            <label class="radio radio-inline">
                                <input type="radio" name="enabled" value="1" {{ Input::old('enabled', $customer ? $customer->enabled : '1') == '1' ? 'checked' : '' }}>
                                Yes
                            </label>
                            <label class="radio radio-inline">
                                <input type="radio" name="enabled" value="0" {{ Input::old('enabled', $customer ? $customer->enabled : '1') == '0' ? 'checked' : '' }}>
                                No
                            </label>
                            {{ implode("\r\n", $errors->get('enabled', '<span class="help-block">:message</span>')) }}
                        </div>
                    </div>
                    <div class="form-actions form-actions-padding-sm">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <button class="btn btn-primary" type="submit">
                                    <i class="icon-save"></i>
                                    Save
                                </button>
                                <a class="btn" type="submit" href="{{ URL::route('customers_list') }}">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop