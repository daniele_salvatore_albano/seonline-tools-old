<?php

class BaseController extends Controller
{
    protected $source;
    protected $me;

    public function __construct()
    {
        $this->layout = $this->getLayoutPath();
        $this->me = Auth::user();

        View::share('controller', $this);
        View::share('me', $this->me);
    }

    public function viewPath($path)
    {
        if (View::exists($path) == false)
        {
            return null;
        }

        return $path;
    }

    protected function getLayoutPath()
    {
    	if (Request::isJson())
    	{
    		return false;
    	}

    	return $this->viewPath('layouts/master');
    }

    protected function view($path)
    {
        if (($fullPath = $this->viewPath($path)) == null)
        {
            if (Config::get('app.debug') == false)
            {
                App::abort(404);
            }
            else
            {
                throw new  Exception('View [' . $path . '] does not exists');
            }
        }

        return View::make($fullPath);
    }

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function ws($successful, $payload = null, $warnings = null, $errors = null)
    {
        return Response::json(array
        (
            'successful'    => $successful,
            'payload'       => $payload,
            'warnings'      => $warnings,
            'errors'        => $errors
        ));
    }

    protected function wsSuccess($payload = null, $warnings = null)
    {
        return $this->ws(true, $payload, $warnings, null);
    }

    protected function wsFailed($errors = null, $payload = null, $warnings = null)
    {
        return $this->ws(false, $payload, $warnings, $errors);
    }
}