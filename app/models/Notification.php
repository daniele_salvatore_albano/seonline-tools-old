<?php

class Notification extends Eloquent {

	protected $table = 'notifications';
	protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo('User');
    }
}