<?php

class Project extends Eloquent {

	protected $table = 'projects';
	protected $softDelete = true;

    public function customer()
    {
        return $this->belongsTo('Customer');
    }

	public static function validate($input)
	{
    	$rules = array
    	(
            'customer'  => array('required', 'exists:customers,id'),
            'name'      => array('required', 'min:4'),
    		'enabled'	=> array('required', 'in:0,1'),
		);

		$messages = array();

    	$validator = Validator::make($input, $rules, $messages);

    	if ($validator->fails())
    	{
    		return $validator;
    	}
	}
}