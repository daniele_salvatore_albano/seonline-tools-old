<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbResourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lb_resources', function($t)
		{
			$t->increments('id');
			$t->integer('user_id')->unsigned()->index();
			$t->string('name');
			$t->string('url');
			$t->string('type')->nullable();
			$t->boolean('require_registration');
			$t->tinyInteger('pagerank')->unsigned()->nullable();
			$t->decimal('mozrank', 5, 2)->unsigned()->nullable();
			$t->decimal('mozrank_subdomain', 5, 2)->unsigned()->nullable();
			$t->tinyInteger('page_authority')->unsigned()->nullable();
			$t->tinyInteger('domain_authority')->unsigned()->nullable();
			$t->integer('last_update')->unsigned();
			$t->text('notes')->nullable();
			$t->boolean('enabled');
			$t->timestamps();
			$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lb_resources');
	}

}
