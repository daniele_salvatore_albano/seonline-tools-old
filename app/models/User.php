<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $table = 'users';
	protected $softDelete = true;
	protected $hidden = array('password');

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getReminderEmail()
	{
		return $this->email;
	}

    public static function validate($input, $current = null)
    {
    	$rules = array
    	(
    		'email' 			=> array('required', 'email', 'unique:users,email,' . ($current ? $current->id : 'NULL') . ',id,deleted_at,NULL'),
    		'first_name' 		=> array('required', 'min:2'),
    		'last_name' 		=> array('required', 'min:2'),
    		'password' 			=> array('required', 'min:6'),
    		'password_verify' 	=> array('required', 'same:password'),
		);

		$messages = array();

    	$validator = Validator::make($input, $rules, $messages);

    	return $validator->messages()->all();
    }
}